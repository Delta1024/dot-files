-- MY CONFIG------|
-- Jacob Stannix--|
-- https://gitlab.com/Delta1024/dot-files/-/blob/master/.config/xmonad/xmonad.hs 
-- ---------------|
-----------
--IMPORTS--
-----------
import Data.Ratio
import XMonad
import XMonad.Actions.FindEmptyWorkspace
import XMonad.Actions.GridSelect
import XMonad.Actions.NoBorders
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.FloatNext
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import System.Exit
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.WorkspaceCompare
import XMonad.Util.Run
import qualified XMonad.StackSet as W

-----------
--LAYOUTS--
-----------
myLayouts = avoidStruts  $ smartBorders $ 
 layoutDefault||| Mirror layoutDefault ||| layoutSpacing Grid ||| layoutSpacing Full ||| noBorders Full  
 where
 layoutTall    = Tall 1 (3/100) (1/2)
 layoutSpacing = spacingRaw False (Border 5 5 5 5) True (Border 5 5 5 5) True 
 layoutDefault = layoutSpacing $ layoutTall
     
-------------------------
--FUNCTION DEFFINITIONS--
-------------------------
windowCount :: X (Maybe String)
windowCount   = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset
myTerminal    = "alacritty"
myModMask     = mod4Mask 
myBorderWidth = 2
myBorderColor = "#228B22"
myEventHook   = handleEventHook def <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook
myWorkspaces  = ["main","2","3","4","5","6","7","8","9"]
headset :: String
headset = "alacritty -e bluetoothctl connect 28:9A:4B:19:05:5B && exit"
----------------
--WINDOW HOOKS--
----------------
myManageHook  = floatNextHook <+> composeAll
  [ className =? "Barrier"                             --> doFloat
  ,(className =? "Brave" <&&> resource =? "brave"    ) --> doFloat --xprop WM_CLASS resource, Class
  -- ,(className =? "Lutris" <&&> resource=? "lutris"   ) --> doFloat
  , className =? "pavucontrol-qt"                      --> doFloat
  ,(className =? "Steam" <&&> title =? "Friends List") --> doFloat  --xprop WM_NAME for title name.
  ,(className =? "Steam" <&&> title =? "Steam - News*") --> doFloat
  ] 
---------------
--KEYBINDINGS--
---------------
myKeybindings = [
-------------------------
--APPLICATION LAUNCHERS--
-------------------------
      ( "M-;"   , spawn "slock"         ) 
    , ( "M-o"   , spawn "emacsclient -c")
    , ( "M-i"   , spawn "brave-bin"     )   
    , ( "M-S-s s" , spawn "steam"         )
    , ( "M-S-s l" , spawn "lutris"        )
    , ( "M-s"   , spawn "pavucontrol-qt")
------------------- 
--SYSTEM CONTROLE--
------------------- 
    , ( "M-M1-<Return>" , spawn "dmenu_run -c -l 20"          ) 
    , ( "M-M1-s s"      , spawn headset                       ) 
    , ( "M-M1-s S-s"    , spawn "alacritty -e bluetoothctl"   )
    , ( "M-S-<Return>"  , spawn "alacritty"                   ) 
    , ( "M-x"           , spawn "alacritty -e ranger"         )
    , ( "M-S-b"         , spawn "killall xmobar"              )
    , ( "M-p"           , spawn "/home/jake/.xmonad/trayer.sh")
    , ( "C-M1-<Delete>" , spawn "alacritty -e bashtop"        )
    , ( "M-g"           , withFocused toggleBorder            )
    , ( "M-C-g"         , toggleWindowSpacingEnabled          )   
    , ( "M-S-g"         , toggleScreenSpacingEnabled          )
    , ( "M-f"           , toggleFloatNext                     )
    , ( "M-S-j"         , goToSelected defaultGSConfig        )
    , ( "M-S-k"         , bringSelected defaultGSConfig       )
    , ( "M-m"           , viewEmptyWorkspace                  )
    , ( "M-S-m"         , tagToEmptyWorkspace                 )
------------------- 
--XRANDR PROFILES--
-------------------
    , ( "M-<F10>" , spawn "/usr/src/scripts/left.sh" )
    , ( "M-<F11>" , spawn "/usr/src/scripts/both.sh" ) 
    , ( "M-<F12>" , spawn "/usr/src/scripts/right.sh") 
    --, ( "M-<F9>" , spawn "/home/jake/.scripts/vm.sh")
-----------    
--SUBMAPS--
-----------
    , ( "M-S-q q" , io (exitWith ExitSuccess))
    , ( "M-S-q r" , spawn "sudo reboot")
    , ( "M-S-q p" , spawn "sudo poweroff")
    ]   
    
-------------
--AUTOSTART--
-------------
myStartupHook = do
    spawnOnce "nitrogen --restore"
    spawnOnce "compton &"
    spawnOnce "pasystray"
    spawnOnce "redshift&"
    spawnOnce "/usr/bin/emacs --daemon"

--------
--MAIN--
--------
main = do
  xmproc0 <- spawnPipe "xmobar -x 0 /home/jake/.xmonad/xmobar0rc"
  xmproc1 <- spawnPipe "xmobar -x 1 /home/jake/.xmonad/xmobar0rc"
  xmonad $ docks desktopConfig 
    { handleEventHook    = myEventHook 
    , terminal           = myTerminal
    , modMask            = myModMask
    , logHook            = dynamicLogWithPP $ def { ppOutput = \x -> hPutStrLn xmproc0 x  >> hPutStrLn xmproc1 x
                                                  , ppCurrent = xmobarColor "#007111" "" . wrap "[" "]"
                                                  , ppVisible = xmobarColor "#228B22" "" -- . wrap "{" "}"
                                                  , ppSort = getSortByXineramaRule
                                                  , ppHidden = xmobarColor "#EA3612" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                                                  , ppHiddenNoWindows = xmobarColor "#008b8b" ""
                                                  , ppUrgent = xmobarColor "#E67E22" "" . wrap "!" "!"
                                                  , ppExtras  = [windowCount]
                                                  , ppSep =  " | "
                                                  , ppTitle = xmobarColor "#b3afc2" "" . shorten 60
                                                  
                                                  }
    , borderWidth        = myBorderWidth
    , focusedBorderColor = myBorderColor
    , layoutHook         = myLayouts
    , manageHook         = myManageHook
    , startupHook        = myStartupHook
    , workspaces         = myWorkspaces
    } `additionalKeysP`  myKeybindings
