git clone --bare https://gitlab.com/Delta1024/dot_files
function config {
   /usr/bin/git --git-dir=$HOME/dot_files/ --work-tree=$HOME $@
}
mkdir -p dot_files
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} dot_files/{}
fi;
config checkout
config config status.showUntrackedFiles no
