----------
--IMPORTS--
-----------
import Data.Ratio
import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Spiral
import XMonad.Layout.Gaps
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.Run

-----------
--LAYOUTS--
-----------
myLayouts = avoidStruts  $ smartBorders $ 
 layoutDefault||| Mirror layoutDefault ||| noBorders Full  
 where
 layoutTall    = Tall 1 (3/100) (1/2)
 layoutSpacing = spacingRaw True (Border 10 10 10 10) False (Border 5 5 5 5) False
 layoutDefault = layoutSpacing $ layoutTall
     
-------------------------
--FUNCTION DEFFINITIONS--
-------------------------
myTerminal    = "alacritty"
myModMask     = mod4Mask 
myBorderWidth = 2
myBorderColor = "#228B22"
myEventHook   = handleEventHook def <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook 
----------------
--WINDOW HOOKS--
----------------
myManageHook  = composeAll
  [ className =? "epicgameslauncher.exe"               --> doFloat
  , className =? "Barrier"                             --> doFloat
  ,(className =? "Brave" <&&> resource =? "brave"    ) --> doFloat --xprop WM_CLASS resource, Class
  ,(className =? "Lutris" <&&> resource=? "lutris"   ) --> doFloat
  , className =? "pavucontrol-qt"                      --> doFloat
  ,(className =? "Steam" <&&> title =? "Friends List") --> doFloat  --xprop WM_NAME for title name.
  ,(className =? "Steam" <&&> title =? "Steam - News (1 of 3)") --> doFloat
  ] 

-------------
--AUTOSTART--
-------------
myStartupHook = do
    spawnOnce "nitrogen --restore"
    spawnOnce "compton &"
    spawnOnce "trayer --edge top --align right --SetDockType true --SetPartialStrut true --expand true --width 5 --height 19 --transparent true --alpha 110 --tint 0x000000 &"
    spawnOnce "pasystray"

--------
--MAIN--
--------
main = do
  xmproc <- spawnPipe "xmobar ~/.config/Xmobar/xmobarrc"
  xmonad $ docks desktopConfig 
    { handleEventHook    = myEventHook 
    , terminal           = myTerminal
    , modMask            = myModMask
    , borderWidth        = myBorderWidth
    , focusedBorderColor = myBorderColor
    , layoutHook         = myLayouts
    , manageHook         = myManageHook
    , startupHook        = myStartupHook
    } 

---------------
--KEYBINDINGS--
---------------
	`additionalKeys`
-------------------------
--APPLICATION LAUNCHERS--
-------------------------
    [ ((mod4Mask .|. shiftMask , xK_l     ) , spawn "slock"    ) 
    , ((mod4Mask               , xK_p     ) , spawn "emacs"    )
    , ((mod4Mask               , xK_i     ) , spawn "brave-bin")   
    , ((mod4Mask .|. mod1Mask  , xK_Return) , spawn "dmenu_run")
------------------- 
--SYSTEM CONTROLE--
------------------- 
    , ((mod4Mask .|. shiftMask                 , xK_Return) , spawn "alacritty"           ) 
    , ((mod4Mask                               , xK_x     ) , spawn "alacritty -e ranger" )
    , ((mod4Mask .|. shiftMask                 , xK_b     ) , spawn "killall xmobar"      )
    , ((controlMask .|. mod1Mask               , xK_Delete) , spawn "alacritty -e bashtop")
    , ((mod4Mask .|. controlMask               , xK_g     ) , toggleWindowSpacingEnabled  ) 
    , ((mod4Mask .|. shiftMask                 , xK_g     ) , toggleScreenSpacingEnabled  )
------------------- 
--XRANDR PROFILES--
-------------------
    , ((mod4Mask , xK_F10) , spawn "/usr/src/scripts/left.sh" )
    , ((mod4Mask , xK_F11) , spawn "/usr/src/scripts/both.sh" ) 
    , ((mod4Mask , xK_F12) , spawn "/urs/src/scripts/right.sh") 
    , ((mod4Mask , xK_F9 ) , spawn "/home/jake/.scripts/vm.sh")
    ]

