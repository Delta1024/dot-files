#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

EDITOR='vim'

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
alias dload="cd /usr/src/dwm; sudo make clean install; cd ~"
export PATH="$HOME/.emacs.d/bin:$PATH" 
export PATH="$HOME/scripts:$PATH"
export PATH="$HOME/.local/bin:$PATH"
alias dconfig="emacs /usr/src/dwm/config.h &"
#alias reboot="dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 org.freedesktop.login1.Manager.Reboot boolean:false"
#alias shutdown="dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 org.freedesktop.login1.Manager.PowerOff boolean:false"
alias lf="/usr/bin/ranger"
alias config="/usr/bin/git --git-dir=$HOME/.dot-files/ --work-tree=$HOME $@"
alias trayer="trayer-srg --edge top --align right --SetDockType true --SetPartialStrut true --expand true --widthtype request --height 19 --tint 0x000000 --monitor 0 --transparent true --alpha 95 &"
neofetch
